const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const axios = require('axios');
const cron = require('node-cron');

const db = require("./models");
const resourcesService = require('./services/resourcesService')(db);
const driversService = require('./services/driversService')(db);
const vehiclesService = require('./services/vehiclesService')(db);

const port = process.argv.slice(2)[0];
const fetchDataInterval = process.argv.slice(2)[1];
const app = express();
app.use(bodyParser.json());

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.get('/getTempResources', (req, res) => {
  res.json(require('./transitInRange_example.json'));
})

const fetchResources = () => {
  axios.get(`http://localhost:${port}/getTempResources`).then(resp => { // Get Resources from local file

    resourcesService.addBulkResources(resp.data)
      .then(res => {
        console.log('Added Resources')
        // console.log(res)
        // res.json(res)
      }).catch(error => {
        console.log('Error adding Resources')
        // console.log(error)
        // res.json(error)
      });

  }).catch(error => {
    console.log('Error getting TEMP Resources')
    // console.log(error)
    // res.json(error)
  });
}

const fetchDrivers = () => {
  axios.get(`http://147.235.178.170:19828/api/Transits/Drivers?fromDate=2021-10-01&toDate=2021-10-02`).then(resp => { // Get Drivers

    driversService.addBulkDrivers(resp.data)
      .then(res => {
        console.log('Added Drivers')
        // console.log(res)
        // res.json(res)
      }).catch(error => {
        console.log('Error adding Drivers')
        // console.log(error)
        // res.json(error)
      });

  }).catch(error => {
    console.log('Error getting Drivers')
    // console.log(error)
    // res.json(error)
  });
}

const fetchVehicles = () => {
  axios.get(`http://147.235.178.170:19828/api/Transits/EtlVehicle?fromDate=2021-10-14&toDate=2021-10-14`).then(resp => { // Get Vehicles

    vehiclesService.addBulkVehicles(resp.data)
      .then(res => {
        console.log('Added Vehicles')
        // console.log(res)
        // res.json(res)
      }).catch(error => {
        console.log('Error adding Vehicles')
        // console.log(error)
        // res.json(error)
      });

  }).catch(error => {
    console.log('Error getting Vehicles')
    // console.log(error)
    // res.json(error)
  });
}

cron.schedule(`*/${fetchDataInterval} * * * *`, function () { // Each 5 hours: 0 */5 * * *
  console.log('Cron executed - ', new Date())
  fetchResources();
  fetchDrivers();
  fetchVehicles();
});

app.use('/img', express.static(path.join(__dirname, 'img')));

console.log(`Microservice for inserting new data listening on port ${port} and I will fetch new data each ${fetchDataInterval} minutes`);
app.listen(port);
