module.exports = (function (db) {
    return {
        addBulkVehicles
    };

    function addBulkVehicles(data) {
        return db.vehicles.bulkCreate(data)
    }

})
