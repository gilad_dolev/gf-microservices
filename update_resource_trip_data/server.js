const express = require('express');
const path = require('path');
const axios = require('axios');
const cron = require('node-cron');
const now = new Date();
const fs = require('fs');

const db = require("./models");
const transitService = require('./services/transitService')(db);
const tripService = require('./services/tripsService')(db);
const resourcesService = require('./services/resourcesService')(db);
const tripToResourceService = require('./services/tripToResourceService')(db);

const port = process.argv.slice(2)[0];
const executeMinutesParam = process.argv.slice(2)[1];
const app = express();

console.log();

if (!port) {
  console.log('ERROR: PORT required! Try: node [file] [port]');
  return;
}

if (!executeMinutesParam) {
  // node server.js 8081 4 // Every 4 minutes
  console.log(`Warning: Cron will NOT be executed. You can run this with get request to http://localhost:${port}/run. If you want to start the cron type: node [file] [port] [executeMinutesParam=1/15]`);
  console.log();
}

const updateResourceTripTimes = async () => {
  const resourcesData = await resourcesService.getAllResources({ is_active: 1 }); // car_number: 7290352 // TEMP
  
  for (let k = 0; k < Object.values(resourcesData).length; k++) {
    let resource = resourcesData[k]
    let tripTimes = await tripService.getTripTimesForResourceId(resource.id);
    // fs.writeFileSync("./temp.json", JSON.stringify(tripTimes));

  //   let tripId = getTripForResource;
  //   tripService.getTrip({id:tripId})
  //   getTripTimes
  //   Sort them
  //   find next time
  //   updateResourceStatus

  }

  console.log(`Resources data should be updated`)

}


let cronTask;

// Cron execution settings
if (executeMinutesParam) {
  let executeMinute = parseInt(executeMinutesParam);
  let cronExecuteFormat = '';

  cronExecuteFormat = `*/${executeMinute} * * * *`; // EVERY X MINUTES
  console.log(`Cron will be executed every ${executeMinute} MINUTES`)

  cronTask = cron.schedule(cronExecuteFormat, function () {
    console.log('Cron executed - ', new Date())
    updateResourceTripTimes();
  });
}

app.get('/run', (req, res) => {
  res.json(updateResourceTripTimes(req.query.date));
})

app.use('/img', express.static(path.join(__dirname, 'img')));

// db.sequelize.sync({ force: true }).then(function () { // drops the tables
//   // db.sequelize.sync({ alter: true }).then(function () { 
//   console.log("Successfully altered DB");
// });

console.log()
console.log(`Microservice updating trip times for resources on port ${port}`);
app.listen(port);
