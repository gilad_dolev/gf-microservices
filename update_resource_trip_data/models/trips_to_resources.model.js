module.exports = function (sequelize, DataTypes) {
    var TripsToResource = sequelize.define("trips_to_resource", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        resourceId: DataTypes.INTEGER,
        tripId: DataTypes.INTEGER
    });

    // TripsToResource.associate = function (models) {
    //     TripsToResource.belongsTo(models.resources, { constraints: false })
    //     TripsToResource.belongsTo(models.transit, { constraints: false })
    // }

    return TripsToResource;
};
