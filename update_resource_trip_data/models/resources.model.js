module.exports = (sequelize, DataTypes) => {
    const Resources = sequelize.define("resources", {
        company_Id: {
            type: DataTypes.INTEGER
        },
        company_code: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        resourceType: {
            type: DataTypes.STRING(45),
            allowNull: false
        },
        resourceSize: {
            type: DataTypes.STRING(45)
        },
        carZone: {
            type: DataTypes.STRING(45)
        },
        line_date: {
            type: DataTypes.DATE,
            allowNull: false
        },
        is_active: {
            type: DataTypes.INTEGER
        },
        line_code: {
            type: DataTypes.INTEGER
        },
        order_start_time: {
            type: DataTypes.TIME,
            allowNull: false
        },
        order_end_time: {
            type: DataTypes.TIME
        },
        order_car_type: {
            type: DataTypes.INTEGER
        },
        line_description: {
            type: DataTypes.STRING(240)
        },
        line_status: {
            type: DataTypes.INTEGER
        },
        line_type: {
            type: DataTypes.INTEGER
        },
        course_code: {
            type: DataTypes.INTEGER
        },
        driverId: { // TO BE ASSOC
            type: DataTypes.INTEGER
        },
        driver_code: {
            type: DataTypes.INTEGER
        },
        driverFirstName: {
            type: DataTypes.STRING(45),
        },
        driverLastName: {
            type: DataTypes.STRING(45),
        },
        driverNickname: {
            type: DataTypes.STRING(45),
        },
        driverPhone: {
            type: DataTypes.STRING(45),
        },
        car_id: {
            type: DataTypes.INTEGER
        },
        car_number: {
            type: DataTypes.STRING(15)
        },
        car_code: {
            type: DataTypes.INTEGER
        },
        carStatus: {
            type: DataTypes.STRING(45)
        },
        carSeats: {
            type: DataTypes.INTEGER
        },
        client_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        client_code: {
            type: DataTypes.INTEGER
        },
        stations: {
            type: DataTypes.TEXT
        },
        comments: {
            type: DataTypes.TEXT
        },
        latitude: {
            type: DataTypes.STRING(45)
        },
        longitude: {
            type: DataTypes.STRING(45)
        },
        position_Last_update: {
            type: DataTypes.DATE
        },
        position_source: {
            type: DataTypes.INTEGER
        },
        position_user_update: {
            type: DataTypes.STRING(45)
        },
        resourceStatus: {
            type: DataTypes.STRING(45),
            defaultValue: 'expected'
        },
        currentTripId: {
            type: DataTypes.INTEGER
        },
        currentTripName: {
            type: DataTypes.STRING(200)
        },
        currentTripStartTime: {
            type: DataTypes.DATE
        },
        currentTripEndTime: {
            type: DataTypes.DATE
        },
        totalTrips: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        date_range: {
            type: DataTypes.STRING(45)
        },
        data_source: {
            type: DataTypes.STRING(45),
            allowNull: false
        },
        // last_update_date_time: {
        //     type: DataTypes.DATE,
        //     allowNull: false
        // },
        tripeDone: {
            type: DataTypes.INTEGER
        }
    });

    // Resources.associate = function (models) {
    //     Resources.belongsToMany(models.trips, {
    //         through: models.trips_to_resource,
    //         constraints: false
    //     });
    //     Resources.hasMany(models.trips_to_resource, { constraints: false });

    // }

    return Resources;
};
